from django.contrib import admin
from .models import (
	Incidence,
	Type
)


@admin.register(Type)
class TypeAdmin(admin.ModelAdmin):
    search_fields = (
    	'name',
    	'key'
	)
    list_display = (
    	'id', 
    	'name', 
    	'key',
	)


@admin.register(Incidence)
class IncidenceAdmin(admin.ModelAdmin):
    search_fields = (
    	'title',
    	'description'
	)
    list_display = (
    	'id', 
    	'title', 
    	'affectation', 
    	'status'
	)

    class Media:
    	css = {
    		'all': ('incidence/css/custom_ckeditor.css',)
    	}
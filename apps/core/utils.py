class FieldView:
    """
    Exclude fields to show.
    """

    no_fields = ()
    context_no_fields_name = 'no_fields'

    def get_no_fields(self):
        return self.no_fields

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if not self.no_fields:
            self.no_fields = self.get_no_fields()
        context[self.context_no_fields_name] = self.no_fields
        return context
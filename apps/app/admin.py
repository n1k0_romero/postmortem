from django.contrib import admin
from .models import App


@admin.register(App)
class AppAdmin(admin.ModelAdmin):
    search_fields = (
    	'name', 
    	'key', 
    	'host'
	)
    list_display = (
    	'id', 
    	'name', 
    	'key', 
    	'host'
	)




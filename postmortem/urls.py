"""postmortem URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import path, include
from apps.core.urls import core_patterns
from apps.app.urls import app_patterns
from apps.incidence.urls import incidence_patterns
from apps.server.urls import server_patterns

urlpatterns = [
    # admin path
	path('caronte/', admin.site.urls),

    # auth paths
    path('accounts/', include('django.contrib.auth.urls')),

    # apps paths
    path('core/', include(core_patterns)),
    path('app/', include(app_patterns)),
	path('incidence/', include(incidence_patterns)),
    path('server/', include(server_patterns)),
]


if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
from django.views.generic.list import ListView
from apps.core.utils import FieldView
from apps.core.viewmixins import DefaultContextMixin
from .models import Server


class ServerListView(DefaultContextMixin, FieldView, ListView):
    model = Server
    template_name = 'core/list.html'
    verbose_title = 'List'
    hidden_fields = (
        'id', 
        'created', 
        'modified', 
        'user_create', 
        'is_active', 
        'ssl', 
        'balance', 
        'ci', 
        'sentry', 
        'monitor', 
        'omniture', 
        'analitics', 
        'comment'
    )

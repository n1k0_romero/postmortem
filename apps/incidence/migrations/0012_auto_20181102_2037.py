# Generated by Django 2.1.1 on 2018-11-02 20:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('incidence', '0011_auto_20181028_1940'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='type',
            name='key',
        ),
        migrations.RemoveField(
            model_name='type',
            name='name',
        ),
    ]

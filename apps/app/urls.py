from django.urls import path
from .views import (
	AppListView, 
	AppDetailView, 
	AppCreateView,
	AppUpdateView
)


app_patterns = ([
    path('list', AppListView.as_view(), name='list'),
    path('<int:pk>/detail', AppDetailView.as_view(), name='detail'),
    path('create', AppCreateView.as_view(), name='create'),
    path('<int:pk>/update', AppUpdateView.as_view(), name='update'),
], 'app')
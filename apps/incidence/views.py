from django.contrib import messages
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from .forms import IncidenceForm
from .models import Incidence


class IncidenceCreateView(CreateView):
    model = Incidence
    form_class = IncidenceForm
    template_name = 'core/form.html'
    success_url = reverse_lazy('core:app_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print(Incidence.back_affectations.pending().count())
        data = {
            'title': self.model.__name__ + '-Create',
            'button_legend': 'Create Incidence',
        }
        context.update(data)
        return context

    def form_valid(self, form):
        form.instance.user_create = self.request.user
        incidence_new = form.save()
        messages.success(self.request, '%s successfully created' %(incidence_new))
        return super().form_valid(form)


class IncidenceListView(ListView):
    model = Incidence
    template_name = 'core/list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        data = {
            'title': self.model.__name__ + '-List',
        }
        context.update(data)
        return context
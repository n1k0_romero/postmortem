from django.contrib import messages
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from apps.core.viewmixins import DefaultContextMixin, DefaultFormValidMixin
from .forms import AppForm
from .models import App


class AppListView(DefaultContextMixin, ListView):
    model = App
    verbose_title = 'List'
    template_name = 'core/list.html'


class AppDetailView(DefaultContextMixin, DetailView):
    model = App
    verbose_title = 'Detail'
    template_name = 'core/detail.html'


class AppCreateView(DefaultContextMixin, CreateView):
    model = App
    form_class = AppForm
    verbose_title = 'Create'
    template_name = 'core/form.html'
    success_url = reverse_lazy('app:list')
    success_message = 'Creada exitosamente'

    def form_valid(self, form):
        form.instance.user_create = self.request.user
        saved_object = form.save()
        messages.success(
            self.request, 
            '%s %s' % (
                saved_object, 
                self.success_message
            )
        )
        return super().form_valid(form)


class AppUpdateView(DefaultContextMixin, DefaultFormValidMixin, UpdateView):
    model = App
    form_class = AppForm
    verbose_title = 'Update'
    template_name = 'core/form.html'
    success_url = reverse_lazy('app:list')
    success_message = 'Actualizada exitosamente'


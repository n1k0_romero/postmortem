from django.db import models
from apps.core.behaviors import (
    ModelFields,
    TimeStampBaseable
)
from apps.core.constants import (
    OPTION_NO,
    OPTION_YES
)


class Server(TimeStampBaseable, ModelFields):
    """
    This class will provides the data
    about the servers.
    """

    YES_NO_OPTIONS = (
        (OPTION_YES, 'SI'),
        (OPTION_NO, 'NO')
    )

    alias = models.CharField(
        max_length=80,
        unique=True,
        help_text='Alias of the server'
    )
    domain = models.CharField(
        max_length=100,
        help_text='Domain of the server'
    )
    pre_production = models.CharField(
        max_length=15,
        help_text='Pre-production IP'
    )
    production = models.CharField(
        max_length=15,
        help_text='Production IP'
    )
    database = models.CharField(
        max_length=40,
        help_text='Database'
    )
    server_db = models.CharField(
        max_length=80,
        help_text='Server DB'
    )
    content_type = models.CharField(
        max_length=50,
        help_text='Content type'
    )
    ssl = models.CharField(
        max_length=1,
        choices=YES_NO_OPTIONS,
        help_text='SSL active'
    )
    balance = models.CharField(
        max_length=1,
        choices=YES_NO_OPTIONS,
        help_text='Load balance'
    )
    ci = models.CharField(
        max_length=1,
        choices=YES_NO_OPTIONS,
        help_text='CI'
    )
    sentry = models.CharField(
        max_length=1,
        choices=YES_NO_OPTIONS,
        help_text='Sentry'
    )
    monitor = models.CharField(
        max_length=1,
        choices=YES_NO_OPTIONS,
        help_text='Monitor'
    )
    omniture = models.CharField(
        max_length=1,
        choices=YES_NO_OPTIONS,
        help_text='Omniture'
    )
    analitics = models.CharField(
        max_length=1,
        choices=YES_NO_OPTIONS,
        help_text='Analitics'
    )
    comment = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        help_text='Comments'
    )

    class Meta:
        verbose_name = "server"
        verbose_name_plural = "servers"

    def __str__(self):
        return '%s' % self.alias
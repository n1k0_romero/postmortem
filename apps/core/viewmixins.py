from django.contrib import messages


class DefaultContextMixin:
    """
    This mixin provides the basic context data.

    Provides:
        - title
        - hidden_fields
        - button_legend
        - url_create
        - ur_update
        - url_detail
        - url_delete

    Note: the model needs a verbose name that matches
          with the name assigned to the url patterns.
    """

    @property    
    def verbose_title(self):
        return '¡WARNING you most implement a verbose_title!'

    @property    
    def hidden_fields(self):
        return '¡WARNING you most implement a hidden_fields!'


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        data = {
            'title': self.model.__name__ + ' ' + self.verbose_title,
            'hidden_fields': self.hidden_fields,
            'button_legend': self.verbose_title + ' ' + self.model.__name__ ,
            'url_create': self.model._meta.verbose_name + ':create',
            'url_update': self.model._meta.verbose_name + ':update',
            'url_detail': self.model._meta.verbose_name + ':detail',
            'url_delete': self.model._meta.verbose_name + ':delete',
        }
        context.update(data)
        return context


class DefaultFormValidMixin:
    """
    This mixin provides the basic acctions for form_valid.

    Provides:
        - user_create
        - success_message
    """

    @property
    def success_message(self):
        return '¡WARNING you most implement a success_message!'
    

    def form_valid(self, form):
        form.instance.user_create = self.request.user
        saved_object = form.save()
        messages.success(
            self.request, 
            '%s %s' % (
                saved_object, 
                self.success_message
            )
        )
        return super().form_valid(form)
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.db import models
from .regex import (
	REGEX_ONLY_LETTERS,
	ONLY_LETTERS_MESSAGE
)
from django.core.exceptions import ValidationError


class TimeStampable(models.Model):
    """
    An abstract base class model that provides self-
    updating ``created`` and ``modified`` fields.
    """

    created = models.DateTimeField(
        auto_now_add=True, 
        null=True, 
        help_text="Time the record was created"
    )
    modified = models.DateTimeField(
        auto_now=True, 
        null=True, 
        help_text="Time the record was modified"
    )

    class Meta:
        abstract = True


class UserCreateable(models.Model):
    """
    An abstract base class model that is provided
    by the user who saved the record.
    """

    user_create = models.ForeignKey(
        User,
        help_text='User who creates the record', 
        on_delete=models.PROTECT
    )

    class Meta:
        abstract = True


class IsActiveable(models.Model):
    """
    An abstract base class model that provides
    if the record is active.
    """

    is_active = models.BooleanField(
        default=True
    )

    class Meta:
        abstract = True


class TimeStampBaseable(TimeStampable, UserCreateable, 
                        IsActiveable):
    """
    An abstract base class model that provides
    the basic timestamp
    
    Provides:
        - created
        - modified
        - user_create
        - is_active
    """

    class Meta:
        abstract = True


class Personable(models.Model):
    """
    An abstract base class model that provides
    the basic fields of a person.
    """

    name = models.CharField(
        max_length=80,
        help_text="Name",
        validators=[
            RegexValidator(
                regex=REGEX_ONLY_LETTERS,
                message=ONLY_LETTERS_MESSAGE,
                code='ONLY_LETTERS'
            )
        ]
    )
    paternal_surname = models.CharField(
        max_length=100,
        help_text="Paternal surname",
        validators=[
            RegexValidator(
                regex=REGEX_ONLY_LETTERS,
                message=ONLY_LETTERS_MESSAGE,
                code='ONLY_LETTERS')
                                
            ]
    )
    maternal_surname = models.CharField(
        max_length=100,
        help_text="Maternal surname",
        validators=[
            RegexValidator(
                regex=REGEX_ONLY_LETTERS,
                message=ONLY_LETTERS_MESSAGE,
                code='ONLY_LETTERS')
                                
            ]
    )
    
    class Meta:
        abstract = True



class ModelFields:
    """Retrieves fields and model values."""

    def get_fields_and_values(self):
        field_list = []

        for field in self.__class__._meta.fields:
            if field.value_to_string(self) == 'None':
                element = [field, '']
            elif field.value_to_string(self) == 'True':
                element = [field, 'Si']
            elif field.value_to_string(self) == 'False':
                element = [field, 'No']
            else:
                element = [field, field.value_to_string(self)]

            field_list.append(element)

        return field_list

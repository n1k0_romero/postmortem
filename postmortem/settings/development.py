from .common import *


THIRD_APPS = [
    'debug_toolbar',
    'ckeditor',
]

OWN_APPS = [
    'apps.core',
    'apps.incidence',
    'apps.app',
    'apps.enviroment',
    'apps.server'
]

INSTALLED_APPS = INSTALLED_APPS + THIRD_APPS + OWN_APPS

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # debug_toobar
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]
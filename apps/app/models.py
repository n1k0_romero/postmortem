from django.db import models
from django.urls import reverse
from apps.core.behaviors import TimeStampBaseable, Personable


class App(TimeStampBaseable):
    """
    This class will provide the data
    about the applications.
    """
    name = models.CharField(
        max_length=100,
        unique=True,
        help_text='name'
    )
    key = models.CharField(
        max_length=5,
        unique=True,
        help_text='key composed by 3 letters'
    )
    host = models.CharField(
        max_length=30
    )
    description = models.TextField()
    tecnologies = models.TextField()

    class Meta:
        verbose_name = "app"
        verbose_name_plural = "apps"

    def __str__(self):
        return '%s' % self.name

    def get_absolute_url(self):
        return reverse('app:detail', kwargs={'pk': self.pk})

    def save(self, *args, **kwargs):
        for field_name in ['name', 'key', ]:
            val = getattr(self, field_name, False)
            if val:
                setattr(self, field_name, val.upper())
        super().save(*args, **kwargs)









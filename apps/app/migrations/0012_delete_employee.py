# Generated by Django 2.1.1 on 2018-11-02 20:50

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0011_employee'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Employee',
        ),
    ]

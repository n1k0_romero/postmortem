# Generated by Django 2.1.1 on 2018-12-08 20:50

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Server',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, help_text='Time the record was created', null=True)),
                ('modified', models.DateTimeField(auto_now=True, help_text='Time the record was modified', null=True)),
                ('is_active', models.BooleanField(default=True)),
                ('alias', models.CharField(help_text='Alias of the server', max_length=80, unique=True)),
                ('domain', models.CharField(help_text='Domain of the server', max_length=100)),
                ('pre_production', models.CharField(help_text='Pre-production IP', max_length=15)),
                ('production', models.CharField(help_text='Production IP', max_length=15)),
                ('database', models.CharField(help_text='Database', max_length=40)),
                ('server_db', models.CharField(help_text='Server DB', max_length=80)),
                ('content_type', models.CharField(help_text='Content type', max_length=50)),
                ('ssl', models.CharField(choices=[('Y', 'SI'), ('N', 'NO')], help_text='SSL active', max_length=1)),
                ('balance', models.CharField(choices=[('Y', 'SI'), ('N', 'NO')], help_text='Load balance', max_length=1)),
                ('ci', models.CharField(choices=[('Y', 'SI'), ('N', 'NO')], help_text='CI', max_length=1)),
                ('sentry', models.CharField(choices=[('Y', 'SI'), ('N', 'NO')], help_text='Sentry', max_length=1)),
                ('monitor', models.CharField(choices=[('Y', 'SI'), ('N', 'NO')], help_text='Monitor', max_length=1)),
                ('omniture', models.CharField(choices=[('Y', 'SI'), ('N', 'NO')], help_text='Omniture', max_length=1)),
                ('analitics', models.CharField(choices=[('Y', 'SI'), ('N', 'NO')], help_text='Analitics', max_length=1)),
                ('comment', models.CharField(blank=True, help_text='Comments', max_length=100, null=True)),
                ('user_create', models.ForeignKey(help_text='User who creates the record', on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]

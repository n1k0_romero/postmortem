from django import forms
from django.core.exceptions import ValidationError
from .models import Incidence 


class IncidenceForm(forms.ModelForm):

	class Meta:
		model = Incidence
		fields = [
			'type',
			'app',
			'enviroment',
			'affectation',
			'status',
			'title',
			'description',
			'solution'
		]
		widgets = {
            'type': forms.Select(
            	attrs={'class':'form-control'}
        	),
            'app': forms.Select(
            	attrs={'class':'form-control'}
        	),
            'enviroment': forms.Select(
            	attrs={'class':'form-control'}
        	),
            'affectation': forms.Select(
            	attrs={'class':'form-control'}
        	),
            'status': forms.Select(
            	attrs={'class':'form-control'}
        	),
            'title': forms.TextInput(
            	attrs={'class':'form-control'}
        	),
            'description': forms.Textarea(
            	attrs={'class':'form-control'}
        	),
            'solution': forms.Textarea(
            	attrs={'class':'form-control'}
        	),
		}

	def clean_title(self):
		# example validate one field
		title = self.cleaned_data.get('title')
		if title == 'test':
			raise ValidationError(
		  		'Error especific field',
			 	code='invalid_title'
		    )
		return title

	def clean(self):
		# example validate entire form
		cleaned_data = super().clean()
		description = self.cleaned_data.get('description')
		solution = self.cleaned_data.get('solution')
		if description == solution:
			# self.add_error('solution', 'Error asociate')
			raise ValidationError(
		  		'Error general form',
			 	code='description_solution_fail'
		 	)
		return cleaned_data
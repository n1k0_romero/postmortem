from django.db import models
from apps.core.behaviors import TimeStampBaseable


class Enviroment(TimeStampBaseable):
    """
    This class will provide the enviroment
    of the incidence.
    """

    name = models.CharField(
        max_length=100,
        unique=True,
        help_text='name'
    )
    key = models.CharField(
        max_length=5,
        unique=True,
        help_text='key composed by 3 letters'
    )

    def __str__(self):
        return '%s' % self.name

    def save(self, *args, **kwargs):
        for field_name in ['name', 'key', ]:
            val = getattr(self, field_name, False)
            if val:
                setattr(self, field_name, val.upper())
        super().save(*args, **kwargs)

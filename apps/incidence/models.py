from django.db import models
from ckeditor.fields import RichTextField
from enum import Enum
from apps.app.models import App
from apps.core.behaviors import TimeStampBaseable
from apps.core.models import Status
from apps.enviroment.models import Enviroment


class Type(TimeStampBaseable):
    """This class will provide the types of incidents."""

    name = models.CharField(
        max_length=100,
        unique=True,
        help_text='name'
    )
    key = models.CharField(
        max_length=5,
        unique=True,
        help_text='key composed by 3 letters'
    )

    def __str__(self):
        return '%s' % self.name

    def save(self, *args, **kwargs):
        for field_name in ['name', 'key', ]:
            val = getattr(self, field_name, False)
            if val:
                setattr(self, field_name, val.upper())
        super().save(*args, **kwargs)


class BackAffectationManager(models.Manager):

    def get_queryset(self):
        back = Incidence.AFECTATION.get_value('back')
        queryset = super().get_queryset()
        queryset = queryset.filter(affectation=back)
        return queryset

    def pending(self):
        queryset = self.get_queryset()
        queryset = queryset.filter(status__key='PEN')
        return queryset


class FrontAffectationManager(models.Manager):

    def get_queryset(self):
        front = Incidence.AFECTATION.get_value('front')
        queryset = super().get_queryset()
        queryset = queryset.filter(affectation=front)
        return queryset


class Incidence(TimeStampBaseable):
    """This class will provide the incidents."""

    class AFECTATION(Enum):
        back = (0, 'BACKEND')
        front = (1, 'FRONTEND')

        @classmethod
        def get_value(cls, member):
            return cls[member].value[0]

    type = models.ForeignKey(
        Type, 
        on_delete=models.PROTECT
    )
    app = models.ForeignKey(
        App, 
        on_delete=models.PROTECT
    )           
    enviroment = models.ForeignKey(
        Enviroment, 
        on_delete=models.PROTECT
    )
    affectation = models.PositiveSmallIntegerField(
        choices=[x.value for x in AFECTATION]
    )
    status = models.ForeignKey(
        Status, 
        on_delete=models.PROTECT
    )
    title = models.CharField(
        max_length=50
    )
    description = RichTextField()
    solution = RichTextField()
    objects = models.Manager()
    back_affectations = BackAffectationManager()
    front_affectations = FrontAffectationManager()

    def __str__(self):
        return '%s - %s' % (self.title, self.status)
    

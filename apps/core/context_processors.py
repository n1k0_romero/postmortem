import django


def get_django_version(request):
	django_version = django.get_version()
	return {'django_version': django_version}
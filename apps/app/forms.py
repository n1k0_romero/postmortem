from django import forms
from .models import App 


class AppForm(forms.ModelForm):

    class Meta:
        model = App
        fields = [
            'name', 
            'key', 
            'host', 
            'description', 
            'tecnologies'
        ]
        widgets = {
            'name': forms.TextInput(
                attrs={'class':'form-control'}
            ),
            'key': forms.TextInput(
                attrs={'class':'form-control'}
            ),            
            'host': forms.TextInput(
                attrs={'class':'form-control'}
            ),
            'description': forms.Textarea(
                attrs={'class':'form-control'}
            ),
            'tecnologies': forms.Textarea(
                attrs={'class':'form-control'}
            ),
        }
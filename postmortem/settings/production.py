from .common import *


DEBUG = False

THIRD_APPS = [
    'ckeditor',
]

OWN_APPS = [
    'apps.core',
    'apps.incidence',
]

INSTALLED_APPS = INSTALLED_APPS + THIRD_APPS + OWN_APPS
from django.urls import path
from .views import (
	ServerListView, 
)


server_patterns = ([
    path('list', ServerListView.as_view(), name='list'),
], 'server')
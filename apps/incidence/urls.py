from django.urls import path
from .views import (
	IncidenceCreateView, 
	IncidenceListView
)


incidence_patterns = ([
    path('create', IncidenceCreateView.as_view(), name='create'),
    path('list', IncidenceListView.as_view(), name='list'),
], 'incidence')
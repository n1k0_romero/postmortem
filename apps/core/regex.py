# regex for text base fields with only letters in spanish
REGEX_ONLY_LETTERS = '^[ a-zA-ZñáéíóúÑÁÉÍÓÚ ]+$'
ONLY_LETTERS_MESSAGE = 'only letters are allowed in this field'